#!/bin/sh

for i in $(seq 0 12); do
    n=$(printf "%02d" $i)
    mkdir reading$n

    cat > reading$n/README.md <<EOF
# Reading $n

https://www3.nd.edu/~pbui/teaching/cse.30341.fa19/reading$n.html
EOF
done
